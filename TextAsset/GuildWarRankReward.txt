TextAsset Base
	string m_Name = "GuildWarRankReward"
	string m_Script = "[
	{"Id": 1, "RankMin": 33, "RankMax": 40, "Reward": 4406},
	{"Id": 2, "RankMin": 41, "RankMax": 50, "Reward": 4407},
	{"Id": 3, "RankMin": 51, "RankMax": 70, "Reward": 4408},
	{"Id": 4, "RankMin": 71, "RankMax": 100, "Reward": 4409},
	{"Id": 5, "RankMin": 101, "RankMax": 140, "Reward": 4410},
	{"Id": 6, "RankMin": 141, "RankMax": 200, "Reward": 4411}
]"
