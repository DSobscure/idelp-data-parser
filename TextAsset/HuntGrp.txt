TextAsset Base
	string m_Name = "HuntGrp"
	string m_Script = "[
	{"Id": 1, "Type": 1, "Name": 401, "Num": 1, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Field", "Bg": "UI-#/Battle/Bg/BG_Bet_Field_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Field_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 2, "Type": 1, "Name": 402, "Num": 2, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Ruins", "Bg": "UI-#/Battle/Bg/BG_Bet_Ruins_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Ruins_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 3, "Type": 1, "Name": 403, "Num": 3, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Castle", "Bg": "UI-#/Battle/Bg/BG_Bet_Castle_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Castle_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 4, "Type": 1, "Name": 404, "Num": 4, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Cliff", "Bg": "UI-#/Battle/Bg/BG_Bet_Cliff_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Cliff_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 5, "Type": 1, "Name": 405, "Num": 5, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Forest", "Bg": "UI-#/Battle/Bg/BG_Bet_Forest_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Forest_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 6, "Type": 1, "Name": 406, "Num": 6, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Lake", "Bg": "UI-#/Battle/Bg/BG_Bet_Lake_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Lake_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 7, "Type": 1, "Name": 407, "Num": 7, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Beach", "Bg": "UI-#/Battle/Bg/BG_Bet_Beach_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Beach_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 8, "Type": 1, "Name": 408, "Num": 8, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Frost", "Bg": "UI-#/Battle/Bg/BG_Bet_Frost_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Frost_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 9, "Type": 1, "Name": 409, "Num": 9, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Garden", "Bg": "UI-#/Battle/Bg/BG_Bet_Garden_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Garden_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 10, "Type": 1, "Name": 410, "Num": 10, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Swamp", "Bg": "UI-#/Battle/Bg/BG_Bet_Swamp_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Swamp_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 11, "Type": 1, "Name": 411, "Num": 11, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Desert_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Desert_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 12, "Type": 1, "Name": 412, "Num": 12, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Volcano", "Bg": "UI-#/Battle/Bg/BG_Bet_Volcano_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Volcano_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 13, "Type": 1, "Name": 413, "Num": 13, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Babarian_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Babarian_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 14, "Type": 1, "Name": 414, "Num": 14, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Despair_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Babarian_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 15, "Type": 1, "Name": 415, "Num": 15, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Nomadic_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Nomadic_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 16, "Type": 1, "Name": 416, "Num": 16, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Meteor_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Meteor_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true},
	{"Id": 17, "Type": 1, "Name": 417, "Num": 17, "GrpBg": "SpriteBg/Stage/UI_Bet_Title_Desert", "Bg": "UI-#/Battle/Bg/BG_Bet_Temple_Day_N", "BgBoss": "UI-#/Battle/Bg/BG_Bet_Temple_Day_N", "Turn": 12, "TurnBoss": 20, "WorldAble": true}
]"
