TextAsset Base
	string m_Name = "ItemPieceCharDecomposition"
	string m_Script = "[
	{"Id": 1, "Grade": 2, "ReqPieceCnt": 1, "CostType": 2, "CostItem": 0, "ReqVal": 1000, "RwdTicketCnt": 5},
	{"Id": 2, "Grade": 3, "ReqPieceCnt": 1, "CostType": 2, "CostItem": 0, "ReqVal": 1200, "RwdTicketCnt": 7},
	{"Id": 3, "Grade": 4, "ReqPieceCnt": 1, "CostType": 2, "CostItem": 0, "ReqVal": 3000, "RwdTicketCnt": 35},
	{"Id": 4, "Grade": 5, "ReqPieceCnt": 1, "CostType": 2, "CostItem": 0, "ReqVal": 20000, "RwdTicketCnt": 140}
]"
